from ket import *
from ket.lib import qft
from ket.plugins import pown
from math import gcd
from random import randint
from functools import reduce

N = 15

a = randint(2, 15-1)
if gcd(a, N) != 1:
    p = N/gcd(a, N)
    q = N/p
    print(p, 'x', q, '=', p*q)
    exit()

results = []

for _ in range(3):
    x = H(quant(N.bit_length()))
    y = pown(a, x, N)

    adj(qft, x)

    results.append(measure(x).get())

r_ = reduce(gcd, results)
r = pow(2, N.bit_length())//r_
p = gcd(a**(r//2)-1, N)
q = gcd(a**(r//2)+1, N)

print(p, 'x', q, '=', p*q, "(quantum)")







