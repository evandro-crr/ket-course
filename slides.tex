\documentclass[aspectratio=169]{beamer}
\usepackage{fontawesome5}
\usepackage{braket}
\usepackage{minted}
\usepackage{tcolorbox}
\usepackage{nicematrix}
\usepackage{newunicodechar}
\usepackage{booktabs}
\usepackage{qcircuit}

\definecolor{ketgreen}{RGB}{0, 173, 159}
\definecolor{ketblue}{RGB}{0, 151, 188}
\definecolor{code_color}{RGB}{251, 255, 176}

\newcommand{\nextline}{$\left.\right.$\newline}
\newcommand{\titleinframe}[1]{{\color{ketgreen}\textbf{#1}}}
\newcommand{\faitem}{\item [\faCaretRight]}
\newunicodechar{≅}{$\cong$}
\newunicodechar{√}{$\surd$}

\setbeamercolor{title}{fg=ketblue}
\setbeamercolor{frametitle}{fg=ketblue}
\setbeamercolor{framesubtitle}{fg=ketgreen}
\setbeamercolor{structure}{fg=ketgreen}

\title{Introduction to Quantum Computing with Ket}
\author{Evandro Chagas Ribeiro da Rosa\\
        \href{mailto:evandro.crr@posgrad.ufsc.br}{evandro.crr@posgrad.ufsc.br}}
\institute{{\normalsize $\bra{\text{G}}\text{C}\ket{\text{Q}}$} \\
Grupo de Computação Quântica - UFSC}
\date{25/10/2021}

\setbeamercolor{footline}{fg=gray}
\setbeamerfont{footline}{series=\bfseries}

\setbeamertemplate{navigation symbols}{
    \usebeamerfont{footline}
    \usebeamercolor[fg]{footline}
    \insertframenumber/25
}

\AtBeginSection{
    \begingroup
    \setbeamertemplate{navigation symbols}{}
    \begin{frame}[noframenumbering]
        \frametitle{\secname}
        \tableofcontents[currentsection]
    \end{frame}
    \endgroup
}

\AtBeginSubsection{
    \begingroup
    \setbeamertemplate{navigation symbols}{}
    \begin{frame}[noframenumbering]
        \frametitle{\subsecname}
        \tableofcontents[currentsubsection]
    \end{frame}
    \endgroup
}

\begin{document}

\begingroup
\setbeamertemplate{navigation symbols}{}
\begin{frame}[noframenumbering]
    \frametitle{IV Workshop de Computação Quântica - UFSC}
    \maketitle
\end{frame}
\endgroup

\begin{frame}
    \frametitle{Agenda}

    \tableofcontents

\end{frame}

\section{Where are we in quantum computing?}

\begin{frame}
    \frametitle{\secname}
    \Large

    \titleinframe{What is Quantum Computing?}

    The use of quantum mechanics phenomena to compute.       

    \begin{itemize}
        \faitem Superposition
        \faitem Entanglement
    \end{itemize}
    
    \vfill

    \titleinframe{Why do we want Quantum Computers?}

    We can solve some problems faster using quantum computing.

    \begin{itemize}
        \faitem Simulate the evolution of quantum systems \cite{feynman1982}
        \faitem Integer factorization problem | Shor's Algorithm \cite{shor1997}
        \faitem Unstructured search problem | Grover's Algorithm \cite{grover1996}
        \faitem Quantum Algorithm Zoo | \href{https://quantumalgorithmzoo.org}{quantumalgorithmzoo.org}
    \end{itemize}

\end{frame}

\begin{frame}[fragile]
    \frametitle{\secname}
    \Large

    \titleinframe{When will Quantum Computers be available?}   

    \begin{itemize}
        \faitem Noisy intermediate-scale quantum (NISQ) era \cite{preskill2018}
        \begin{itemize}
            \item [\faAws] Amazon Braket
            \item [\faMicrosoft] Azure Quantum
        \end{itemize}
        \faitem Quantum advantage \cite{Arute2019}
        \faitem Fault-tolerant quantum computers \cite{devitt2013}
    \end{itemize}

    \titleinframe{How to program and execute a quantum algorithm?}
    \begin{itemize}
        \faitem Quantum programming libraries
        \begin{itemize}
            \faitem Cirq
            \faitem Qiskit
        \end{itemize} 
        \faitem IBM Quantum Experience | \href{https://quantum-computing.ibm.com}{quantum-computing.ibm.com}
        \faitem Ket programming language | \href{https://quantum-ket.gitlab.io}{quantum-ket.gitlab.io} \cite{ket}

    \end{itemize}

\end{frame}

\section{A quantum bit in the Bloch sphere}

\begin{frame}[fragile]
    \frametitle{\secname}
    \begin{center}
        \includegraphics[scale=1.4]{bola/bola0.pdf}
    \end{center}
\end{frame}


\begin{frame}[fragile]
    \frametitle{\secname}
    \begin{center}
        \includegraphics[scale=1.4]{bola/bola1.pdf}
    \end{center}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\secname}
    \begin{center}
        \includegraphics[scale=1.4]{bola/bola2.pdf}
    \end{center}
    Complex number: $z = x+iy$ for $x, y \in \mathbb{R}$ and $i = \sqrt{-1}$
\end{frame}

\begin{frame}[fragile]
    \frametitle{\secname}
    \Large
    \centering
    \titleinframe{Pauli X gate}

    \begin{minipage}[t]{.49\linewidth}
        \includegraphics{bola/bolaz0.pdf}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{.49\linewidth}
        \includegraphics{bola/bolaz1.pdf}
    \end{minipage}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\secname}
    \Large
    \centering
    \titleinframe{Pauli Y gate}

    \begin{minipage}[t]{.49\linewidth}
        \includegraphics{bola/bolax0.pdf}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{.49\linewidth}
        \includegraphics{bola/bolax1.pdf}
    \end{minipage}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\secname}
    \Large
    \centering
    \titleinframe{Pauli Z gate}

    \begin{minipage}[t]{.49\linewidth}
        \includegraphics{bola/bolay0.pdf}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{.49\linewidth}
        \includegraphics{bola/bolay1.pdf}
    \end{minipage}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\secname}
    \Large
    \centering
    \titleinframe{Hadamard gate}

    \begin{minipage}[t]{.49\linewidth}
        \includegraphics{bola/bolaz0.pdf}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{.49\linewidth}
        \includegraphics{bola/bolax0.pdf}
    \end{minipage}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\secname}
    \Large
    \centering
    \titleinframe{Hadamard gate}

    \begin{minipage}[t]{.49\linewidth}
        \includegraphics{bola/bolaz1.pdf}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{.49\linewidth}
        \includegraphics{bola/bolax1.pdf}
    \end{minipage}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\secname}
    \Large
    \titleinframe{Quantum measurement}
    \begin{center}
        \includegraphics[scale=1.4]{bola/bolam.pdf}    
    \end{center}    
\end{frame}

\section{Qubits in the Computational basis}

\begin{frame}
    \frametitle{\secname}

    \Large

    \begin{minipage}{.49\linewidth}
        \begin{itemize}
            \faitem $z_{+1}$ $\ket0$
            \faitem $z_{-1}$ $\ket1$
            \faitem $x_{+1}$ $\frac{1}{\sqrt{2}}\left(\ket0+\ket1\right) = \ket+$
            \faitem $x_{-1}$ $\frac{1}{\sqrt{2}}\left(\ket0-\ket1\right) = \ket-$
            \faitem $y_{+1}$ $\frac{1}{\sqrt{2}}\left(\ket0+i\ket1\right)$
            \faitem $y_{-1}$ $\frac{1}{\sqrt{2}}\left(\ket0-i\ket1\right)$
            \faitem $\ket\psi = \alpha\ket0+\beta\ket1$,
            
            $|\alpha|^2+|\beta|^2 = 1$, 
            
            $\alpha, \beta \in \mathbb{C}$
        \end{itemize}
    \end{minipage}
    \hfill
    \begin{minipage}{.49\linewidth}
        \includegraphics[width=\linewidth]{bola/bolam.pdf}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Global Phase}
    \large

    \begin{align*}
        \ket{\psi} = \alpha\ket0+\beta\ket1   & \equiv & -\ket\psi = -\alpha\ket0-\beta\ket1  & \equiv & i\ket\psi = i\alpha\ket0+i\beta\ket1 \\
                  \frac{\ket0+\ket1}{\sqrt2}  & \equiv & \frac{-\ket0-\ket1}{\sqrt2}          & \equiv & \frac{i\ket0+i\ket1}{\sqrt2}  \\
                  \frac{\ket0-\ket1}{\sqrt2}  & \equiv & \frac{-\ket0+\ket1}{\sqrt2}          & \equiv & \frac{i\ket0-i\ket1}{\sqrt2}  \\
                  \frac{\ket0+i\ket1}{\sqrt2} & \equiv & \frac{-\ket0-i\ket1}{\sqrt2}         & \equiv & \frac{i\ket0-\ket1}{\sqrt2} \\
                  \frac{\ket0-i\ket1}{\sqrt2} & \equiv & \frac{-\ket0+i\ket1}{\sqrt2}         & \equiv & \frac{i\ket0+\ket1}{\sqrt2} 
    \end{align*}

\end{frame}

\begin{frame}
    \frametitle{\secname}
    \titleinframe{\Large Quantum gates}
    \vfill
    \large
    \begin{minipage}{.42\linewidth}
        \begin{itemize}
            \faitem $\begin{array}{lcr}
                X\ket0 & = & \ket1 \\
                X\ket1 & = & \ket0            
            \end{array}$
            \faitem $\begin{array}{lcr}
                Y\ket0 & = & -i\ket1 \\
                Y\ket1 & = &  i\ket0            
            \end{array}$
            \faitem $\begin{array}{lcr}
                Z\ket0 & = & \ket0 \\
                Z\ket1 & = & -\ket1            
            \end{array}$
            \faitem $\begin{array}{lcr}
                H\ket0 & = & \frac{1}{\sqrt{2}}\left(\ket0+\ket1\right)\\
                H\ket1 & = & \frac{1}{\sqrt{2}}\left(\ket0-\ket1\right)            
            \end{array}$
            \faitem $\begin{array}{lcr}
                \text{phase}(\lambda)\ket0 & = & \ket0 \\
                \text{phase}(\lambda)\ket1 & = & e^{i\lambda}\ket1            
            \end{array}$        
        \end{itemize}
    \end{minipage}
    \hfill
    \begin{minipage}{.57\linewidth}
        \begin{itemize}
            \faitem $\begin{array}{lcr}
                S\ket0 & = & \ket0 \\
                S\ket1 & = & i\ket1            
            \end{array}$
            \faitem $\begin{array}{lcr}
                T\ket0 & = & \ket0 \\
                T\ket1 & = & \frac{1+i}{\sqrt{2}}\ket1            
            \end{array}$
            \faitem $\begin{array}{lcrr}
                RX(\theta)\ket0 & = & \cos\frac{\theta}{2}\ket0   &-i\sin\frac{\theta}{2}\ket1 \\
                RX(\theta)\ket1 & = & -i\sin\frac{\theta}{2}\ket0 & +\cos\frac{\theta}{2}\ket1 
            \end{array}$ 
            \faitem $\begin{array}{lcrr}
                RY(\theta)\ket0 & = & \cos\frac{\theta}{2}\ket0   &\sin\frac{\theta}{2}\ket1 \\
                RY(\theta)\ket1 & = & -\sin\frac{\theta}{2}\ket0 & +\cos\frac{\theta}{2}\ket1 
            \end{array}$ 
            \faitem $\begin{array}{lcr}
                RZ(\theta)\ket0 & = & e^{-i\theta/2}\ket0 \\
                RZ(\theta)\ket1 & = & e^{i\theta/2}\ket1 
            \end{array}$ 

        \end{itemize}
    \end{minipage}  
\end{frame}


\begin{frame}[fragile]
    \frametitle{Hands on}

    \Large

    \begin{minipage}[t]{.49\linewidth}
        \titleinframe{Print a qubit}
        \begin{minted}{ket}
from ket import *
from math import pi
q = quant()
H(q)
print(dump(q).show())
    \end{minted}
    \vspace{12pt}
    \begin{minted}[fontsize=\small]{text}
|0⟩		(50%)
0.707107                 ≅  1/√2

|1⟩		(50%)
0.707107                 ≅  1/√2        
        \end{minted}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{.49\linewidth}
        \titleinframe{Exercises}

        Print the states: 

        \begin{itemize}
            \faitem $\frac{1}{\sqrt{2}}\left(\ket0+\ket1\right)$
            \faitem $\frac{1}{\sqrt{2}}\left(\ket0-\ket1\right)$
            \faitem $\frac{1}{\sqrt{2}}\left(\ket0-i\ket1\right)$
            \faitem $\frac{1}{\sqrt{2}}\ket0+\frac{1-i}{2}\ket1$
            \faitem $\frac{1}{\sqrt{2}}\ket0-\frac{1+i}{2}\ket1$
            \faitem $\sqrt{\frac{1}{4}}\ket{0}+\sqrt{\frac{3}{4}}\ket{1} \cong  0.5\ket0+0.866025\ket1 $
        \end{itemize}
    \end{minipage}     
\end{frame}

\begin{frame}[fragile]
    \frametitle{\secname}
    \Large
    \titleinframe{Quantum measurement}
    \vfill
    \begin{minipage}[t]{.49\linewidth}
        \begin{itemize}
            \faitem $\ket\psi = \alpha\ket0+\beta\ket1$
            \faitem $p(0, \ket\psi) = |\alpha|^2$
            \faitem $p(1, \ket\psi) = |\beta|^2$
            \faitem $|\alpha|^2+|\beta|^2=1$
        \end{itemize}  

        \begin{itemize}
            \faitem $p(0, \frac{1}{\sqrt{2}}\ket0+\frac{1}{\sqrt{2}}\ket1) = 0.5$
            \faitem $p(1, \frac{1}{\sqrt{2}}\ket0-\frac{i}{\sqrt{2}}\ket1) = 0.5$
            \faitem $p(1, \frac{1}{\sqrt{2}}\ket0-\frac{1+i}{2}\ket1) = 0.5$
            \faitem $p(0, \sqrt{\frac{1}{4}}\ket{0}+\sqrt{\frac{3}{4}}\ket{1}) = 0.25$

        \end{itemize}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{.49\linewidth}
        \begin{minted}{ket}
from ket import *
from math import pi
m = {0 : 0, 1 : 0}
for _ in range(100):
    q = quant()
    H(q)
    m[measure(q).get()] += 1
print(m)
        \end{minted}
        \vfill
        \begin{minted}[fontsize=\small]{text}
{0: 52, 1: 48}
        \end{minted}
    \end{minipage}
    
    

\end{frame}

\begin{frame}[fragile]
    \frametitle{\secname}
    \Large
    \titleinframe{Quantum entanglement}
    \vfill
    \begin{minipage}[c]{.49\linewidth}
    
        \begin{minted}{ket}
from ket import *
a = quant()
b = quant()
H(a)
with control(a):
    X(b)
ma = measure(a)
mb = measure(b)
print(ma.get(), mb.get())
        \end{minted}
    \end{minipage}
    \hfill
    \begin{minipage}[c]{.49\linewidth}
        \titleinframe{Exercises}  

        Print and measure the states:
        \begin{itemize}
            \faitem $\frac{1}{\sqrt{2}}\left(\ket{00}+\ket{11}\right)$
            \faitem $\frac{1}{\sqrt{2}}\left(\ket{01}+\ket{10}\right)$
            \faitem $\frac{1}{\sqrt{2}}\left(\ket{00}-\ket{11}\right)$
            \faitem $\frac{1}{\sqrt{2}}\left(\ket{01}-\ket{10}\right)$
            \faitem $\frac{1}{\sqrt{3}}\left(\ket{00}+\ket{10}+\ket{11}\right)$
            \faitem $\frac{1}{\sqrt{3}}\left(\ket{00}+\ket{01}+\ket{10}\right)$
        \end{itemize}
    \end{minipage}   
\end{frame}


\begin{frame}[fragile]
    \frametitle{\secname}
    \Large
    \titleinframe{Quantum entanglement}
    \vfill
    \large
    \begin{minipage}[c]{.3\linewidth}
        \Large
        \begin{minted}{ket}
from ket import *
def bell(a, b): 
    H(a)
    ctrl(a, X, b)
a, b = quant(2)
bell(a, b)
X(b)
adj(bell, a, b)
print(dump(a+b).show())
        \end{minted}
    \end{minipage}
    \hfill
    \begin{minipage}[c]{.55\linewidth}
        \titleinframe{GZH-State:}
        \begin{minted}{ket}
q = quant(20)
ctrl(H(q[0]), X, q[1:])
        \end{minted}
        \vfill
        \titleinframe{W-State:}
        \begin{minted}{ket}
from math import asin, sqrt
n = 10
q = quant(n)
X(q[0])
for i in range(n-1):
    G = RY(2*asin(sqrt((n-i-1)/(n-i))))
    ctrl(q[i], G, q[i+1])
    cnot(q[i+1], q[i])
        \end{minted}
        
    \end{minipage}   
\end{frame}

\section{The mathematical formalism of quantum computing}

\begin{frame}[fragile]
    \frametitle{\secname}
    \begin{center}
        
        \begin{tabular}{c l}
            \toprule
            Notation & Description  \\
            $A^\dagger$ & Conjugate transpose of matrix $A$, $A^\dagger = {(A^T)}^*$ \\ 
            $\ket{\psi}$ & Column vector; pronounced ``ket $\psi$'' \\
            $\bra{\psi}$ & Dual vector of $\ket{\psi}$; pronounced ``bra $\psi$''; $\bra{\psi} = \ket{\psi}^\dagger$ \\
            $\braket{\varphi|\psi}$ & Inner product between $\ket{\varphi}$ and $\ket{\psi}$ \\
            $\ket{\varphi}\otimes\ket{\psi}$ & Tensor product between $\ket{\varphi}$ and $\ket{\psi}$ \\
            $\ket{\varphi}\ket{\psi}$ & Tensor product between $\ket{\varphi}$ and $\ket{\psi}$ \\
            $\ket{\varphi\psi}$ & Tensor product between $\ket{\varphi}$ and $\ket{\psi}$ \\
            \bottomrule
        \end{tabular}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{State space}
    \framesubtitle{\secname}
    \large

    \textbf{Postulate 1}: Associated to any isolated physical system is a
    complex vector space with inner product (that is, a Hilbert space) known
    as the \textit{state space} of the system. The system is completely
    described by its \textit{state vector}, which is a unit vector in the
    system’s state space. \cite[p. 80]{Nielsen2010}

    \begin{align*}
        \ket{0} = \begin{bNiceMatrix}
            1 \\ 0
        \end{bNiceMatrix} &&
        \ket{1} = \begin{bNiceMatrix}
            0 \\ 1
        \end{bNiceMatrix}
    \end{align*}

    \begin{align*}
        \alpha\ket{0}+\beta\ket{1} & = \begin{bNiceMatrix}
            \alpha \\ \beta
        \end{bNiceMatrix}\\
        \frac{1}{\sqrt{2}}\left(\ket{0}+\ket{1}\right) & = \frac{1}{\sqrt{2}} \begin{bNiceMatrix}
            1 \\ 1
        \end{bNiceMatrix}
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Evolution}
    \large
    \textbf{Postulate 2}: The evolution of a \textit{closed} quantum system is
    described by a \textit{unitary} transformation. That is, the state
    $\ket{\psi}$ of the system at time $t_1$ is related to the state
    $\ket{\psi'}$ of the system at time $t_2$ by a unitary operator $U$ which
    depends only on the times $t_1$ and $t_2$,
    \begin{equation}
        \ket{\psi'} = U\ket{\psi}.
    \end{equation} \cite[p. 81]{Nielsen2010}

    \begin{align*}
        X & = \begin{bNiceMatrix}
            0 & 1 \\ 1 & 0
        \end{bNiceMatrix} & 
        Y & = \begin{bNiceMatrix}
            0 & -i \\ i & 0
        \end{bNiceMatrix}\\
        Z & = \begin{bNiceMatrix}
            1 & 0 \\
            0 & -1
        \end{bNiceMatrix} &
        H & = \frac{1}{\sqrt{2}}\begin{bNiceMatrix}
            1 & 1 \\ 1 & -1
        \end{bNiceMatrix}
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Evolution}
    \large
    \begin{align*}    
        X\Ket{0} & = \begin{bNiceMatrix}
            0 & 1 \\ 1 & 0
        \end{bNiceMatrix}\begin{bNiceMatrix}
            1 \\ 0 
        \end{bNiceMatrix} = \begin{bNiceMatrix}
            0 \\ 1 
        \end{bNiceMatrix} &  
        X\Ket{1} & = \begin{bNiceMatrix}
            0 & 1 \\ 1 & 0
        \end{bNiceMatrix}\begin{bNiceMatrix}
            0 \\ 1 
        \end{bNiceMatrix} = \begin{bNiceMatrix}
            1 \\ 0 
        \end{bNiceMatrix}\\
        Y\Ket{0} & = \begin{bNiceMatrix}
            0 & -i \\ i & 0
        \end{bNiceMatrix}\begin{bNiceMatrix}
            1 \\ 0 
        \end{bNiceMatrix} = \begin{bNiceMatrix}
            0 \\ -i 
        \end{bNiceMatrix} &  
        Y\Ket{1} & = \begin{bNiceMatrix}
            0 & -i \\ i & 0
        \end{bNiceMatrix}\begin{bNiceMatrix}
            0 \\ 1 
        \end{bNiceMatrix} = \begin{bNiceMatrix}
            i \\ 0 
        \end{bNiceMatrix} \\
        Z\Ket{0} & = \begin{bNiceMatrix}
            1 & 0 \\ 0 & -1
        \end{bNiceMatrix}\begin{bNiceMatrix}
            1 \\ 0 
        \end{bNiceMatrix} = \begin{bNiceMatrix}
            1 \\ 0 
        \end{bNiceMatrix} & 
        Z\Ket{1} & = \begin{bNiceMatrix}
            1 & 0 \\ 0 & -1
        \end{bNiceMatrix}\begin{bNiceMatrix}
            0 \\ 1 
        \end{bNiceMatrix} = \begin{bNiceMatrix}
            0 \\ -1 
        \end{bNiceMatrix}\\
        H\Ket{0} & = \frac{1}{\sqrt{2}}\begin{bNiceMatrix}
            1 & 1 \\ 1 & -1
        \end{bNiceMatrix}\begin{bNiceMatrix}
            1 \\ 0 
        \end{bNiceMatrix} = \frac{1}{\sqrt{2}}\begin{bNiceMatrix}
            1 \\ 1 
        \end{bNiceMatrix} & 
        H\Ket{1} & = \frac{1}{\sqrt{2}}\begin{bNiceMatrix}
            1 & 1 \\ 1 & -1
        \end{bNiceMatrix}\begin{bNiceMatrix}
            0 \\ 1 
        \end{bNiceMatrix} = \frac{1}{\sqrt{2}}\begin{bNiceMatrix}
            1 \\ -1 
        \end{bNiceMatrix}
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Quantum Measurement}
    \large
    \textbf{Postulate 3}: Quantum measurements are described by a collection
    $\{M_m\}$ of \textit{measurement operators}. These are operators acting
    on the state space of the system being measured. The index $m$ refers to
    the measurement outcomes that may occur in the experiment. If the state
    of the quantum system is $\ket{\psi}$ immediately before the measurement
    then the probability that result $m$ occurs is given by
    \begin{equation}
        p(m) = \bra{\psi}M_m^\dagger M_m\ket{\psi},
        \label{eq:qc:pm}
    \end{equation}
    and the state of the system after the measurement is
    \begin{equation}
        \frac{M_m\ket{\psi}}{\sqrt{\bra{\psi}M_m^\dagger M_m\ket{\psi}}}.
        \label{eq:qc:after}
    \end{equation}
    The measurement operators satisfy the \textit{completeness equation},
    \begin{equation}
        \sum_m M_m^\dagger M_m = I.
    \end{equation} \cite[p. 84]{Nielsen2010}
\end{frame}

\begin{frame}
    \frametitle{Quantum Measurement}

    \large

    \begin{align*}
        \ket\psi = \alpha\ket{0}+\beta\ket{1} &&
        M_0 = \ket0\!\bra0 = \begin{bNiceMatrix} 1 & 0 \\ 0 & 0 \end{bNiceMatrix} && 
        M_1 = \ket1\!\bra1 = \begin{bNiceMatrix} 0 & 0 \\ 0 & 1 \end{bNiceMatrix}
    \end{align*}
    
    \begin{align*}
        p(M_0) & = \left(\alpha^\dagger\bra{0}+\beta^\dagger\bra{1}\right)\ket0\!\braket{0|0}\!\bra0\left(\alpha\ket{0}+\beta\ket{1}\right) \\
        & = \left(\alpha^\dagger\braket{0|0}+\beta^\dagger\braket{1|0}\right)\braket{0|0}\left(\alpha\braket{0|0}+\beta\braket{0|1}\right) \\
        & = \alpha^\dagger\alpha = |\alpha|^2
    \end{align*}

    \begin{align*}
        \frac{M_0\ket{\psi}}{\sqrt{\bra{\psi}M_0^\dagger M_0\ket{\psi}}} 
        & = \frac{\ket0\!\bra0\left(\alpha\ket{0}+\beta\ket{1}\right)}{\sqrt{|\alpha|^2}} \\
        & = \frac{\ket0\left(\alpha\braket{0|0}+\beta\braket{0|1}\right)} {|\alpha|} = \frac{\alpha\ket0} {|\alpha|} 
    \end{align*}
    

\end{frame}

\begin{frame}
    \frametitle{Composite systems}
    \large
    \textbf{Postulate 4}: The state space of a composite physical system is the
    \textit{tensor product} of the state spaces of the component physical
    systems. Moreover, if we have systems numbered 1 through $n$, and system
    number $i$ is prepared in the state $\ket{\psi_i}$, then the joint state of
    the total system is
    $\ket{\psi_1}\otimes\ket{\psi_2}\otimes\cdots\otimes\ket{\psi_n}$. \cite[p.
    92]{Nielsen2010}

    \begin{equation*}
        A \otimes B = \begin{bNiceMatrix}
            A_{11}B & A_{12}B & \Cdots  & A_{1n}B  \\
            A_{11}B & A_{12}B & \Cdots  & A_{1n}B  \\
            A_{21}B & A_{22}B & \Cdots  & A_{2n}B  \\
            \Vdots  & \Vdots  & \Ddots & \Vdots    \\
            A_{m1}B & A_{m2}B & \Cdots  & A_{mn}B  
        \end{bNiceMatrix}_{mp \times nq}
    \end{equation*} 
\end{frame}

\begin{frame}
    \frametitle{Composite systems}

    \large

    \begin{align*}
        \text{CNOT}(H_0\ket{00}) & = \frac{1}{\sqrt{2}}\left(\ket{00}+\ket{11}\right)
    \end{align*}

    \begin{align*}
        \left(\frac{1}{\sqrt{2}}\begin{bNiceMatrix}
            1 & 1 \\ 1 & -1
        \end{bNiceMatrix}\otimes\begin{bNiceMatrix}
            1 & 0 \\ 0 & 1
        \end{bNiceMatrix}\right)\left(\begin{bNiceMatrix}
            1 \\ 0 
        \end{bNiceMatrix}\otimes\begin{bNiceMatrix}
            1 \\ 0
        \end{bNiceMatrix}\right) & = \frac{1}{\sqrt{2}} \begin{bNiceMatrix}
            1 & 0 & 1 & 0 \\
            0 & 1 & 0 & 1 \\
            1 & 0 & -1 & 0 \\
            0 & 1 & 0 & -1 
        \end{bNiceMatrix}\begin{bNiceMatrix}
            1 \\ 0 \\ 0 \\ 0 
        \end{bNiceMatrix} = \frac{1}{\sqrt{2}}\begin{bNiceMatrix}
            1 \\ 0 \\ 1 \\ 0 
        \end{bNiceMatrix} \\
        \begin{bNiceMatrix}
            1 & 0 & 0 & 0 \\
            0 & 1 & 0 & 0 \\
            0 & 0 & 0 & 1 \\
            0 & 0 & 1 & 0
        \end{bNiceMatrix} \frac{1}{\sqrt{2}}\begin{bNiceMatrix}
            1 \\ 0 \\ 1 \\ 0 
        \end{bNiceMatrix} & = \frac{1}{\sqrt{2}}\begin{bNiceMatrix}
            1 \\ 0 \\ 0 \\ 1 
        \end{bNiceMatrix}
    \end{align*}

\end{frame}

\section{Quantum algorithms}

\begin{frame}[fragile]
    \frametitle{Quantum Circuit}
    \Large

    \begin{minipage}[c]{.46\linewidth}
        \begin{align*}
            \text{Pauli X: }          & \Qcircuit @C=1em @R=.7em { & \gate{X} & \qw } \text{ } \Qcircuit @C=1em @R=.7em { & \targ & \qw } \\
            \text{Pauli Y: }          & \Qcircuit @C=1em @R=.7em { & \gate{Y} & \qw } \\
            \text{Pauli Z: }          & \Qcircuit @C=1em @R=.7em { & \gate{Z} & \qw } \\
            \text{Hadamard: }         & \Qcircuit @C=1em @R=.7em { & \gate{H} & \qw } \\
            \text{phase($\lambda$): } & \Qcircuit @C=1em @R=.7em { & \gate{e^{i\lambda}} & \qw } \\
            \text{RX($\theta$): }      & \Qcircuit @C=1em @R=.7em { & \gate{R_x\theta} & \qw } \\
            \text{RY($\theta$): }      & \Qcircuit @C=1em @R=.7em { & \gate{R_y\theta} & \qw } \\
            \text{RZ($\theta$): }      & \Qcircuit @C=1em @R=.7em { & \gate{R_z\theta} & \qw }
        \end{align*}
    \end{minipage}
    \hfill
    \begin{minipage}[c]{.49\linewidth}
        \begin{align*}
            \text{CNOT: }     & \Qcircuit @C=1em @R=.7em { & \targ & \qw \\ & \ctrl{-1} & \qw } \\%\text{ } \Qcircuit @C=1em @R=.7em {& \targ & \qw \\ & \ctrl{-1} & \qw}
            \text{Toffoli: }  & \Qcircuit @C=1em @R=.7em { & \ctrl{2} & \qw \\ & \ctrl{1} & \qw \\ & \targ & \qw }\\
            \text{Controlled-Y: }     & \Qcircuit @C=1em @R=.7em { & \gate{Y} & \qw \\ & \ctrl{-1} & \qw } \\%\text{ } \Qcircuit @C=1em @R=.7em {& \targ & \qw \\ & \ctrl{-1} & \qw}
            \text{Measure: }  & \Qcircuit @C=1em @R=.7em { & \meter & \cw } \\
            \text{Classical control: }     & \Qcircuit @C=1em @R=.7em { & \gate{e^{i\lambda}} & \qw \\ & \cctrl{-1} & \cw } 
        \end{align*}
    \end{minipage}

\end{frame}

\begin{frame}
    \frametitle{Quantum Teleportation}
    \huge
    \[
        \Qcircuit @C=.8em @R=.4em {
            \lstick{\ket{\psi}} & \ctrl{1} & \gate{H} & \meter & \cw       & \cctrl{1}     & \\
                                & \targ    & \qw      & \meter & \cctrl{1} &               & \\
                                & \qw      & \qw      & \qw    & \gate{X}  & \gate{Z} \cwx & \rstick{\ket{\psi}} \qw
            \inputgroupv{2}{3}{.6em}{.8em}{\ket{\beta_{00}}}\\
        }
    \]

    \begin{equation*}
        \ket{\beta_{00}} = \frac{\ket{00}+\ket{11}}{\sqrt{2}}
    \end{equation*}

\end{frame}

\begin{frame}[fragile]
    \frametitle{Quantum Teleport}

    \begin{minipage}[t]{.49\linewidth}
    \begin{minted}{ket}
from ket import *
from ket import code_ket
from math import pi

def bell():
    q = quant(2)
    H(q[0])
    cnot(q[0], q[1])
    return q
    \end{minted}
    \end{minipage}
    \begin{minipage}[t]{.49\linewidth}
        \begin{minted}{ket}
@code_ket
def telport(psi):
    alice, bob = bell()
    cnot(psi, alice)
    H(psi)
    m_psi = measure(psi)
    m_alice = measure(alice)
    if m_alice == 1:
        X(bob)
    if m_psi == 1:
        Z(bob)
    return bob

psi = quant()
RY(2*pi/3, psi)
bob = telport(psi)
        \end{minted}
        \end{minipage}
    

\end{frame}

\begin{frame}
    \frametitle{Grover's Algorithm}

    \Large
    \begin{equation*}
        \Qcircuit @C=.8em @R=.4em {
            &&&& \mbox{Repeat $\pi/4\sqrt{2^n}$ times} & \\ \\ \\
                             & \gate{H}  & \multigate{3}{\text{Oracle}} & \gate{H} & \multigate{2}{2\ket{0}\!\bra{0}^{\otimes n} - I^{\otimes n}} & \gate{H} & \meter \\
                             & \cdots    & \nghost{\text{Oracle}}       & \cdots   & \nghost{2\ket{0}\!\bra{0}^{\otimes n} - I^{\otimes n}}       & \cdots   & \cdots \\
                             & \gate{H}  & \ghost{\text{Oracle}}        & \gate{H} & \ghost{2\ket{0}\!\bra{0}^{\otimes n} - I^{\otimes n}}        & \gate{H} & \meter \\
            \lstick{\ket{-}} & \qw       & \ghost{\text{Oracle}}        & \qw      & \qw                                                          & \qw      & \qw 
            \gategroup{4}{3}{4}{6}{.8em}{^\}}
            \inputgroupv{4}{6}{.6em}{.8em}{\ket{0}^{\otimes n}}
        }
    \end{equation*}
        
    \centering
    $\ket{-} = \frac{\ket{0}-\ket{1}}{\sqrt{2}}$
    \begin{align*}
        U\ket{x}\ket{y} = \ket{x}\ket{f(x)\oplus y} 
        &&
        f:\{0, 1, \dots, 2^n-1\} \rightarrow \{0, 1\}
    \end{align*}    
\end{frame}

\begin{frame}[fragile]
    \frametitle{Grover's Algorithm}
    \begin{minipage}[t]{.49\linewidth}
    \begin{minted}{ket}
from ket import *
from math import pi, sqrt

def oracle(x, y):
    with control(x, on_state=3):
        X(y)

n = 2
x = H(quant(n))
y = H(X(quant()))
    \end{minted}
    \end{minipage}
    \begin{minipage}[t]{.49\linewidth}
    \begin{minted}{ket}
for _ in range(int(pi/4*sqrt(2**n))):
    oracle(x, y)
    with around(H, x):
        with around(X, x):
            ctrl(x[1:], Z, x[0])

print(measure(x).get())
            \end{minted}
        \end{minipage}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Shor's Algorithm}
    \Large

    \begin{equation*}
        \Qcircuit @C=.8em @R=.4em {
        & \gate{H}  & \multigate{5}{y = a^x \mod N} & \multigate{2}{QFT^\dagger} & \multigate{2}{\metersymb} \\
        & \cdots    & \nghost      {y = a^x \mod N} & \nghost      {QFT^\dagger} & \nghost      {\metersymb} & \rstick{r'} \cw \\
        & \gate{H}  & \ghost       {y = a^x \mod N} & \ghost       {QFT^\dagger} & \ghost       {\metersymb} \\
        & \qw       & \ghost       {y = a^x \mod N} & \qw                        &        \\
        & \cdots    & \nghost      {y = a^x \mod N} & \cdots                              \\
        & \qw       & \ghost       {y = a^x \mod N} & \qw                          
        \inputgroupv{1}{3}{.6em}{.8em}{\ket{x}}
        \inputgroupv{4}{6}{.6em}{.8em}{\ket{y}}
        }
    \end{equation*}    
\end{frame}

\begin{frame}[fragile]
    \frametitle{Shor's Algorithm}

    \begin{minipage}[t]{.43\linewidth}
        \begin{minted}{ket}
from ket import *
from ket.lib import qft
from ket.plugins import pown
from math import gcd
from random import randint
from functools import reduce

N = 15

a = randint(2, 15-1)
if gcd(a, N) != 1:
    p = N/gcd(a, N)
    q = N/p
    print(p, 'x', q, '=', p*q)
    exit()         
        \end{minted}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{.53\linewidth}
        \begin{minted}{ket}
results = []

for _ in range(3):
    x = H(quant(N.bit_length()))
    y = pown(a, x, N)

    adj(qft, x)

    results.append(measure(x).get())

r_ = reduce(gcd, results)
r = pow(2, N.bit_length())//r_
p = gcd(a**(r//2)-1, N)
q = gcd(a**(r//2)+1, N)

print(p, 'x', q, '=', p*q, "(quantum)")            
                    \end{minted}
    \end{minipage}

    
\end{frame}

\begingroup
\setbeamertemplate{navigation symbols}{}
\begin{frame}
    \frametitle{Thank you for your attention}
    \maketitle  
\end{frame}
\endgroup

\begin{frame}[t,allowframebreaks]
    \frametitle{References}
    \bibliographystyle{alpha}
    \bibliography{slides}
\end{frame}
    

\end{document}